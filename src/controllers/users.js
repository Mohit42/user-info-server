user = require('../model/user');
const express = require('express');
const router = express.Router();

router.post('/', (req, res) => {
    const params = req.body;
    var userData = new user({
        name: params.name,
        email: params.email,
        place: params.place,
        budget: params.budget,
        count: params.count
    });
    userData.save().then(item => {
        res.status(200).send(JSON.stringify('Success'));
    }).catch(err => {
        console.log(err);
        next(new Error());
    });
});

router.get('/', (req, res) => {
    const email = req.query.email;
    console.log(email);
    user.findOne({ email: email },{_id: 0, __v: 0}).then(item => {
        res.status(200).send(item);
    }).catch(err => {
        console.log(err);
        next(new Error());
    });
});

module.exports = router;