const mongoose = require('mongoose');
var useerSchema = new mongoose.Schema({
    name: String,
    email: {
        type: String,
        required: true,
        unique: true
    },
    place: String,
    count: Number,
    budget: Number
});

var user = mongoose.model('user', useerSchema);
module.exports = user;