const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const app = express();
const { PORT } = require('./shared/config.json');
const userRoute = require('./controllers/users');
const {
    generalError
} = require('./middlewares/error');
app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));
app.use(cors());

mongoose.connect('mongodb://0.0.0.0:27017/travelopiaUserInfo', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
});
console.log('started');

app.use('/user', userRoute);

app.use(generalError);

app.listen(PORT);