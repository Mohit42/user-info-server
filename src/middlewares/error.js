module.exports = {
    generalError: function(err,req,res,next){
        res.status(err.statusCode || 400).send(err.status || 'Error');        
    }
}